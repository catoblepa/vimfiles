           _                    
          (_)                   
    __   ___ _ __ ___  _ __ ___ 
    \ \ / / | '_ ` _ \| '__/ __|
     \ V /| | | | | | | | | (__ 
      \_/ |_|_| |_| |_|_|  \___|

    catoblepa VIM environment


Installation
============

Clone in your home:
`git clone https://bitbucket.org/catoblepa/vimfiles.git .vim`

Grab the plugin submodules:
`cd ~/.vim && git submodule init && git submodule update`

Link .vimrc file:
`ln -s ~/.vim/vimrc ~/.vimrc`

Create directories
`mkdir ~/.vim/_backup && mkdir ~/.vim/_temp`

Update submodules
`git submodule foreach git pull origin master`
