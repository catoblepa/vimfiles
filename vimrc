"        _                    
"       (_)                   
" __   ___ _ __ ___  _ __ ___ 
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__ 
"   \_/ |_|_| |_| |_|_|  \___|
"                             
" catoblepa VIM environment


" General settings
execute pathogen#infect()

" Defaults
set encoding=utf-8
let &t_Co=256                     " more colors
set clipboard=unnamedplus,unnamed " use system clipboard
set nocompatible                  " nocompatible is good for humans
syntax enable                     " enable syntax highlighting...
filetype plugin indent on         " depending on filetypes...
set pastetoggle=<F12>             " for pasting code into Vim
set autoread                      " auto load files if vim detects change
" set noswapfile
set history=500                   " remember more commands and search history
set undolevels=500
set title

"" Style
set background=dark
set ruler                         " show the cursor position all the time
set cursorline                    " enable cursor line
set showcmd                       " display incomplete commands
set novisualbell                  " no flashes please
set scrolloff=3                   " provide some context when editing
set hidden                        " Allow backgrounding buffers without writing them, and
                                  " remember marks/undo for backgrounded buffers
" Mouse
set mousehide                     " hide mouse when writing
"set mouse=a                       " we love the mouse

" Whitespace
" set nowrap                        " don't wrap lines
set tabstop=4                     " a tab is two spaces
set shiftwidth=4                  " an autoindent (with <<) is two spaces
set softtabstop=4                 " when deleting, treat spaces as tabs
set expandtab                     " use spaces, not tabs
set backspace=indent,eol,start    " backspace through everything in insert mode
set autoindent                    " keep indentation level when no indent is found

" Wild life
set wildmenu                      " wildmenu gives autocompletion to vim
set wildmode=list:longest,full    " autocompletion shouldn't jump to the first match
set wildignore+=tmp/**,*.rbc,.rbx,*.scssc,*.sassc,*.csv,*.pyc,*.xls

" List chars
" characters used to represent invisibles
set lcs+=tab:»\         " 187 followed by a space (032)
set lcs+=eol:¶          " 182
set lcs+=trail:·        " 183
if has("win32")
	set lcs+=extends:  " 133
	set lcs+=precedes: " 133
elseif has("unix")
	set lcs+=extends:_  " underscore (095)
	set lcs+=precedes:_ " underscore (095)
endif

"" Searching
set hlsearch                      " highlight matches
set incsearch                     " incremental searching
set ignorecase                    " searches are case insensitive...
set smartcase                     " unless they contain at least one capital letter

" Windows
set splitright                    " create new horizontal split on the right
set splitbelow                    " create new vertical split below the current window
" set winwidth=84

" Backup and status line
set backupdir=~/.vim/_backup    " where to put backup files.
set directory=~/.vim/_temp      " where to put swap files.
set backup
set writebackup
set laststatus=2

"" FileType settings
if has("autocmd")
  " in Makefiles use real tabs, not tabs expanded to spaces
  augroup filetype_make
    au!
    au FileType make setl ts=8 sts=8 sw=8 noet
  augroup END

  " make sure all markdown files have the correct filetype set and setup wrapping
  augroup filetype_markdown
    au!
    au BufRead,BufNewFile *.{md,mdown,mkd,mkdn,markdown,mdwn}   set filetype=markdown
	au BufRead,BufNewFile *.{md,mdown,mkd,mkdn,markdown,mdwn}.{des3,des,bf,bfa,aes,idea,cast,rc2,rc4,rc5,desx} set filetype=markdown
  augroup END

  " treat JSON files like JavaScript
  augroup filetype_json
    au!
    au BufNewFile,BufRead *.json setf javascript
  augroup END

  " make Python follow PEP8 ( http://www.python.org/dev/peps/pep-0008/ )
  augroup filetype_django
    au!
    au FileType python,htmldjango,html setl sts=4 ts=4 sw=4
  augroup END

  " enable <CR> in command line window and quickfix
  augroup enable_cr
    au!
    au CmdwinEnter * nnoremap <buffer> <CR> <CR>
    au BufWinEnter quickfix nnoremap <buffer> <CR> <CR>
  augroup END

  " disable automatic comment insertion
  augroup auto_comments
    au!
    au FileType * setlocal formatoptions-=ro
  augroup END

  " remember last location in file, but not for commit messages,
  " or when the position is invalid or inside an event handler,
  " or when the mark is in the first line, that is the default
  " position when opening a file. See :help last-position-jump
  augroup last_position
    au!
    au BufReadPost *
      \ if &filetype !~ '^git\c' && line("'\"") > 1 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif
  augroup END
endif

" Mappings
let mapleader=','

" disable man page for word under cursor
nnoremap K <Nop>

" y u consistent?
nnoremap Y y$

" clear the search buffer when hitting return
nnoremap <CR> :nohlsearch<CR>

" expand %% to current directory
cnoremap %% <C-R>=expand('%:h').'/'<CR>
nmap <Leader>e :e %%

" toggle list mode
nnoremap <silent> <Leader>l :set nolist!<CR>

" easy substitutions
nnoremap <Leader>r :%s///gc<Left><Left><Left><Left>

" remove whitespaces and windows EOL
command! KillWhitespace :normal :%s/\s\+$//e<CR><C-O><CR>
command! KillControlM :normal :%s/<C-V><C-M>//e<CR><C-O><CR>
nnoremap <Leader>W :KillWhitespace<CR>

"" Plugins configuration
let g:NERDTreeMouseMode = 3
let g:NERDTreeHighlightCursorline = 0




" SET
set browsedir=last
set spelllang=it,en
set switchbuf="usetab"
set autochdir
set number


" configure VIM for using ctrl-c ctrl-v as copy paste (extracted from mswin.vim)
" CTRL-X -> Cut
vnoremap <C-X> "+x
" CTRL-C -> Copy
vnoremap <C-C> "+y
" CTRL-V -> Paste
map <C-V> "+gP
cmap <C-V> <C-R>+
" Pasting blockwise and linewise selections is not possible in Insert and
" Visual mode without the +virtualedit feature.  They are pasted as if they
" were characterwise instead.
" Uses the paste.vim autoload script.
exe 'inoremap <script> <C-V>' paste#paste_cmd['i']
exe 'vnoremap <script> <C-V>' paste#paste_cmd['v']
" Use CTRL-B to do what CTRL-V used to do (visual bloc)
noremap <C-B> <C-V>


" GVIM
set guifont=Monaco\ 10
" set guifont=Monospace\ 11
set guioptions-=T	" hide toolbar
set guioptions-=m	" hide menu
" set guioptions-=r	" hide right scroll bar
nnoremap ,m :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>


" MAP
map ,b :ls!<CR>:e #
map ,t :tabs<CR>
"map <F6> :tabfirst <CR>
map <F7> :tabprev <CR>
map <F8> :tabnext <CR>
map <C-W><C-T> :tabnew <CR>
map <F3> :BufExplorer<cr>
map <F9> :set spell! <CR>
" map <F10> :SuperTabHelp <CR>
let g:SuperTabMappingTabLiteral = '<s-tab>'

map ,co :!git --git-dir="../.APPUNTI.git" add -A && git --git-dir="../.APPUNTI.git" commit -m "update"<CR>

" ABBREV
iabbrev { {}<Left>
iabbrev <% <% %><Left><Left><Left>

" AUTOCMD
" autocmd FileType * set tabstop=4|set shiftwidth=4|set noexpandtab
autocmd FileType ruby set tabstop=2|set shiftwidth=2|set expandtab
"autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab
autocmd! BufNewFile * silent! 0r ~/.vim/Templates/template.%:e


" MENÙ PERSONALIZZATI
:menu 80.11 Plugin.Custom.Apri\ Shell :!shell <CR>
:menu 80.12 Plugin.Custom.Show\ file :!ls -lh % <CR>
:menu 80.13 Plugin.Custom.Show\ PWD :!pwd <CR>
:menu 80.14 Plugin.Custom.Anteprima\ HTML\ (pandoc) :!pandoc % > /tmp/output.html && xdg-open /tmp/output.html & <CR>
:menu 80.15 Plugin.Custom.-separator- :
:menu 80.19 Plugin.Custom.Reload\ vimrc :source ~/.vimrc <CR>
:menu 80.20 Plugin.Spell.Spell\ IT :set spl=it spell <CR>
:menu 80.21 Plugin.Spell.Spell\ EN :set spl=en spell <CR>
:menu 80.22 Plugin.Spell.Spell\ OFF :set nospell <CR>
:menu 80.30 Plugin.Templates.Bash :0read ~/.vim/Templates/template.sh <CR>
:menu 80.31 Plugin.Templates.Python :0read ~/.vim/Templates/template.py <CR>
:menu 80.32 Plugin.Templates.Ruby :0read ~/.vim/Templates/template.rb <CR>


" *************************
" PLUGINS
" *************************

" NERDTree
map <F4> :NERDTreeToggle<cr>
" let NERDTreeQuitOnOpen=1
let NERDTreeMinimalUI=1
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" Tagbar
map <F2> :TagbarToggle<cr>
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'ctagsbin' : '~/.vim/bundle/markdown2ctags/markdown2ctags.py',
    \ 'ctagsargs' : '-f - --sort=yes',
    \ 'kinds' : [
        \ 's:sections',
        \ 'i:images'
    \ ],
    \ 'sro' : '|',
    \ 'kind2scope' : {
        \ 's' : 'section',
    \ },
    \ 'sort': 0,
    \ }

" Netrw
let  g:netrw_altv=1
let g:netrw_browse_spit=    2

" Markdown
let g:vim_markdown_folding_disabled=1
" let g:vim_markdown_initial_foldlevel=3


" *************************
" COLORI
" *************************
colorscheme molokai
" colorscheme solarized
hi search guibg=yellow guifg=black ctermbg=11 ctermfg=0
